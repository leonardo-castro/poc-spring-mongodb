package demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.model.document.DemoDocument;
import demo.model.service.DemoService;

/**
 * @RestController will add "@ResponseBody" to every method declaration, making our methods returns 
 * just as specified on its return type.
 * 
 * E.g.: Instead of returning a "ModelAndView" object as used to do in a convencional @Controller, we are now
 * able to return a primitive "String" type, in that case, our fully Json retrived from the server 
 * */
@RestController
public class DemoController {

	/**
	 * @Autowired will make Spring auto inject this private class for our use 
	 * */
	@Autowired
	private DemoService demoService;
	
	@RequestMapping(value="/demo", method = RequestMethod.GET)
	public String getDemoByName(
			@RequestParam(value="name", required = true) String name) {
		return demoService.getDemoByName(name).toString();
	}
	
	/**
	 * Uses "produces" attribute to allow Jackson to automactilly converts your method return type
	 * to a JSON object using the annotations specified on that domain class.
	 * That way, @JsonProperty can work properly. 
	 * IMPORTANT: Its necessary to send a Header attribute <Key, Value> on the request as follows:
	 * 
	 * Header			Value
	 * ------			-----
	 * Content-Type		application/json
	 * */
	@RequestMapping(value="/demo/{id}", method = RequestMethod.GET, produces="application/json")
	public DemoDocument getDemoById(
			@PathVariable String id) {
		return demoService.getDemoById(id);
	}
	
	/**
	 * Sending a complete "DemoDocument" object makes "demoRepository.save(demoDocument)" to update the full object.
	 * The "PUT - updateDemo" and "POST - newDemo" makes the same thing, since the repository.save() 
	 * also creates and update objects.
	 * */
	@RequestMapping(value="/demo", method = RequestMethod.PUT, consumes="application/json")
	public String updateDemo(@RequestBody DemoDocument demoDocument) {
		demoService.createOrUpdateDemo(demoDocument);
		return HttpStatus.OK.toString();
	}
	
	/**
	 * Uses "consumes" attribute to allow POST requests with Json body texts.
	 * IMPORTANT: Its necessary to send a Header attribute <Key, Value> on the request as follows:
	 * 
	 * Header			Value
	 * ------			-----
	 * Content-Type		application/json
	 * */
	@RequestMapping(value="/demo", method = RequestMethod.POST, consumes="application/json")
	public String newDemo(@RequestBody DemoDocument demoDocument) {
		demoService.createOrUpdateDemo(demoDocument);
		return HttpStatus.OK.toString();
	}
	
	@RequestMapping(value="/demo/{id}", method = RequestMethod.DELETE, consumes="application/json")
	public String deleteDemo(@PathVariable String id) {
		demoService.deleteDemo(id);
		return HttpStatus.OK.toString();
	}
	
}
