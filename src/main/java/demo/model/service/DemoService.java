package demo.model.service;

import java.util.List;

import demo.model.document.DemoDocument;

public interface DemoService {

	public DemoDocument getDemoById(String id);
	public List<DemoDocument> getDemoByName(String name);
	public void createOrUpdateDemo(DemoDocument demoDocument);
	public void deleteDemo(String id);
	
}
