package demo.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.model.document.DemoDocument;
import demo.model.repository.DemoRepository;
import demo.model.service.DemoService;

@Service("demoService")
public class DemoServiceImpl implements DemoService {

	@Autowired
	private DemoRepository demoRepository;
	
	@Override
	public List<DemoDocument> getDemoByName(String name) {
		return demoRepository.findByName(name);
	}

	@Override
	public void createOrUpdateDemo(DemoDocument demoDocument) {
		demoRepository.save(demoDocument); 
	}

	@Override
	public DemoDocument getDemoById(String id) {
		return demoRepository.findById(id);
	}
	
	@Override
	public void deleteDemo(String id) {
		demoRepository.delete(id);
	}

}
