package demo.model.document;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonProperty;

import demo.model.document.embedded.ComplexObjectEmbedded;

/**
 * db.demo.find({name:"pão de batata"})
 *		|	|		| 
 *		|	| 		|
 *		|	|	 document 
 *		|	|	 attribute
 *		|  find
 *		|function
 *		|
 * 	 collection
 * */
@Document(collection = "demo")
public class DemoDocument {

	/**
	 * Id is auto generated to the Document, you dont need to specify it when inserting a new Document on MongoDB
	 * */
	@Id
	private String id;
	private String name;
	
	/**
	 * @Field Use it when you want to map Java complex objects that have different names on the MongoDB Json Object.
	 * For example: 
	 * 
	 * db.demo.insert({
		name:"test 2 complexObject",
		complexObject: [{
			name:"complexObject Name",
			age:"20"
		},
		{
			name:"complexObject 2",
			age:"55"
		}]
	   });
	 *  
	 * That way you can map your class as "ComplexObjectEmbedded", and refer to it on the MongoDB Json as "complexObject"
	 * 
	 * @JsonProperty Makes possible to @RequestBody controller methods to parse the "complexObject" JSON attribute
	 * to the Java attribute "complexObjectEmbedded".
	 * If we dont use the @JsonProperty, it would be needed to send the "DemoDocument" json with the 
	 * "complexObjectEmbedded" attribute, as follows:
	 *  {
			"name":"test 2 complexObject",
			"complexObjectEmbedded": [{
				"name":"complexObject Name",
				"age":"20"
			},
			{
				"name":"complexObject 2",
				"age":"55"
			}]
		}
	 * */
	@JsonProperty(value="complexObject")
	@Field(value="complexObject")
	private List<ComplexObjectEmbedded> complexObjectEmbedded;

	/**
	 * GETTERS & SETTERS
	 * */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ComplexObjectEmbedded> getComplexObjectEmbedded() {
		return complexObjectEmbedded;
	}

	public void setComplexObjectEmbedded(
			List<ComplexObjectEmbedded> complexObjectEmbedded) {
		this.complexObjectEmbedded = complexObjectEmbedded;
	}
	
}
