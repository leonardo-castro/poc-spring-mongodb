package demo.model.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import demo.model.document.DemoDocument;

/**
 * Extending the "MongoRepository" on the Repository, is going to allow us to connect
 * on the MongoDB server specified on the "application.properties" on "spring.data.mongodb.uri"
 * */
public interface DemoRepository extends MongoRepository<DemoDocument, String> {

	/**
	 * "?0" means that the query attribute "name" must concat the 
	 * first method "findByName" parameter to that query, in that case, the "String name" parameter
	 * */
	@Query(value = "{name: ?0}")
	public List<DemoDocument> findByName(String name);
	
	/**
	 * Defining a method with specific keywords generate automatic MongoDB queries.
	 * Check the list of available query methods at(session "Table 5.1"):
	 * http://docs.spring.io/spring-data/data-mongo/docs/1.4.2.RELEASE/reference/html/mongo.repositories.html
	 * */
	public DemoDocument findById(String id);
	
}
